# wp-config.php

A wp-config file that fits this project universally for any environment.

```php
<?php
/**
 * This is the configuration file for WordPress.
 * This file should never be commited.
 */

// Environment.
define( 'ARB_PRODUCTION', false );

// MySQL Settings.
define( 'DB_NAME', 'db_name' );
define( 'DB_USER', 'db_user' );
define( 'DB_PASSWORD', 'db_password' );
define( 'DB_HOST', '127.0.0.1' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

$table_prefix = 'prjwp_'; // phpcs:ignore

// WordPress Debug.
define( 'WP_DEBUG', true );

// Set WP Content URLs.
$server_name       = $_SERVER['SERVER_NAME'] ?? ''; // phpcs:ignore
$http_protocol     = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://';
$relative_pageroot = '';

define( 'WP_DIRECTORY', $http_protocol . $server_name . $relative_pageroot );
define( 'WP_ROOT_DIR', dirname( __FILE__ ) );
define( 'WP_CONTENT_DIR', WP_ROOT_DIR . '/_content' );
define( 'WP_CONTENT_URL', WP_DIRECTORY . '/_content' );

// Dynamically set WP URLs.
define( 'WP_SITEURL', $http_protocol . $server_name );
define( 'WP_HOME', $http_protocol . $server_name );

/* That's all, stop editing! Happy publishing. */

if ( WP_DEBUG ) {
	define( 'WP_DEBUG_DISPLAY', true );
	define( 'WP_DEBUG_LOG', true );
	define( 'SCRIPT_DEBUG', true );
	define( 'SAVEQUERIES', true );
}

/**#@+
* Please generate Salts!
* https://api.wordpress.org/secret-key/1.1/salt/
*/

/* !!!GENERATE YOUR SALTS!!! */

/**#@-*/

if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

require_once ABSPATH . 'wp-settings.php';
```
